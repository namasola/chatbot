import requests

def classify(preg):
    key = "887cef70-71b1-11e9-9cb8-87d6a71cb439aff6c477-89d9-4285-abbd-e1238347cab8"
    url = "https://machinelearningforkids.co.uk/api/scratch/"+ key + "/classify"

    response = requests.get(url, params={ "data" : preg }) #Get:Obtener datos que sean valiosas de una url

    if response.ok:
        #ṕrint(response.content) #mostrar contenido con content
        datos_respuesta = response.json()
        categoria = datos_respuesta[0]
        return categoria #diccionario que guarda la categoria con mayor credibilidad
    else:
        response.raise_for_status()

def responder(res,preg):#Funcion que permite comprobar si la pregunta corresponde a algunas de las etiquetas y ver su probabilidad
    if(res["class_name"] == "genero" and res["confidence"] > 50):#Si la pregunta si es de esa etiqueta se responde correctamente
        print("La pelicula Narnia es de género fantasía.")
    elif(res["class_name"] == "sinopsis" and res["confidence"] > 50):
        print("La pelicula Narnia trata sobre cuatro niños que viajan a través de un ropero a la tierra de Narnia, donde vivirán increíbles aventuras con la ayuda del león Aslan.")
    elif(res["class_name"] == "reparto" and res["confidence"] > 50):
        print("El reparto en la película Narnia esta formado por William Moseley, Anna Popplewell, Skandar Keynes, Georgie Henley, Liam Neeson, Tilda Swinton, Klará Issova")
    elif(res["class_name"] == "en_que_esta_basado" and res["confidence"] > 50):
        print("La película Narnia esta basada en la serie de novelas homónima escrita por C.S. Lewis.")
    elif(res["class_name"] == "estreno" and res["confidence"] > 50):
        print("La película Narnia se estrenó el 9 de diciembre de 2005 en Estados Unidos y fue filmada en Nueva Zelanda.")

    else:
        print("No puedo responder esta pregunta, la inteligencia necesita mas entrenamiento ;(. Perdon x naser. O tambien estas preguntando cualquier cosa >:(")

while (True):
        preg=input("Ingrese una pregunta: ")
        res = classify(preg)
        responder(res,preg)















