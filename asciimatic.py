from asciimatics.effects import Cycle, Stars
from asciimatics.renderers import FigletText
from asciimatics.scene import Scene
from asciimatics.screen import Screen
from PIL import ImageFile, Image
#class asciimatics.renderers.ImageFile("/home/alumno/chatbot/narnia.jpg", height=30, colours=8)


def demo(screen):
    
    effects = [
        Cycle(
            screen,
            FigletText("Narnia", font='big'),
            int(screen.height / 2 - 4)),
#        Cycle(
#            screen,
#            FigletText("!", font='big'),
#            int(screen.height / 2 + 3)),
        Stars(screen, 1000)
    ]
    screen.play([Scene(effects, 400)])


Screen.wrapper(demo)

